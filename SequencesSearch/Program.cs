﻿using static SequencesSearch.SequencesSearch;

if (args.Length > 0)
{
    foreach (var arg in args)
    {
        int result = CountMaxUnequalChars(arg);
        Console.WriteLine("{0} - {1}", arg, result.ToString());
    }
}
else
{
    Console.WriteLine("No arguments");
}