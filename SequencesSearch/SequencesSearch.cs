﻿namespace SequencesSearch
{
    public class SequencesSearch
    {
        //determining the maximum number of unequal consecutive characters
        public static int CountMaxUnequalChars(string s)
        {
            List<int> maxChars = new();
            int count = 0, len = s.Length;

            if (len <= 1)
            {
                maxChars.Add(len);
            }
            else
            {
                count++;

                for (int i = 1; i < len; i++)
                {
                    if (s[i] != s[i - 1])
                    {
                        count++;
                    }
                    else
                    {
                        maxChars.Add(count);
                        count = 1;
                    }

                    if (i == len - 1)
                    {
                        maxChars.Add(count);
                    }
                }
            }

            return maxChars.Max();
        }

        //determining the maximum number of consecutive identical letters of the Latin alphabet in a line
        public static int CountMaxIdenticalLetters(string s)
        {
            List<int> maxChars = new();
            int count = 0, len = s.Length;

            if (len <= 1)
            {
                maxChars.Add(len > 0 && char.IsLetter(s[0]) ? 1 : 0);
            }
            else
            {
                for (int i = 1; i < len; i++)
                {
                    if (s[i] == s[i - 1] && char.IsLetter(s[i]))
                    {
                        count += count == 0 ? 2 : 1;
                    }
                    else
                    {
                        maxChars.Add(count);
                        count = 0;
                    }

                    if (i == len - 1)
                    {
                        maxChars.Add(count);
                    }
                }
            }

            return maxChars.Max();
        }

        //determining the maximum number of consecutive identical digits
        public static int CountMaxIdenticalDigits(string s)
        {
            List<int> maxChars = new();
            int count = 0, len = s.Length;

            if (len <= 1)
            {
                maxChars.Add(len > 0 && char.IsDigit(s[0]) ? 1 : 0);
            }
            else
            {
                for (int i = 1; i < len; i++)
                {
                    if (s[i] == s[i - 1] && char.IsDigit(s[i]))
                    {
                        count += count == 0 ? 2 : 1;
                    }
                    else
                    {
                        maxChars.Add(count);
                        count = 0;
                    }

                    if (i == len - 1)
                    {
                        maxChars.Add(count);
                    }
                }
            }

            return maxChars.Max();
        }
    }
}
