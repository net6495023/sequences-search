﻿using NUnit.Framework;
using static SequencesSearch.SequencesSearch;

namespace SequencesSearchTest
{
    [TestFixture]
    public class SequencesSearchTests
    {
        [TestCase("", ExpectedResult = 0)]
        [TestCase("a", ExpectedResult = 1)]
        [TestCase(" ", ExpectedResult = 1)]
        [TestCase("abc", ExpectedResult = 3)]
        [TestCase("123", ExpectedResult = 3)]
        [TestCase("abcdef", ExpectedResult = 6)]
        [TestCase("1a2b3c_d", ExpectedResult = 8)]
        [TestCase("abcdefghijklmnop", ExpectedResult = 16)]
        public int? CountMaxUnequalChars_EqualCharactersDoNotExist_ReturnsLenghtOfString(string line)
        {
            return CountMaxUnequalChars(line);
        }

        [TestCase("aaa", ExpectedResult = 1)]
        [TestCase(" aab bb", ExpectedResult = 4)]
        [TestCase("abcde11", ExpectedResult = 6)]
        [TestCase("abc deffghi 1234567", ExpectedResult = 12)]
        [TestCase("abcdefghij   lmnop", ExpectedResult = 11)]
        public int? CountMaxUnequalChars_EqualCharactersExist_ReturnsCorrectResults(string line)
        {
            return CountMaxUnequalChars(line);
        }

        [TestCase("", ExpectedResult = 0)]
        [TestCase("1", ExpectedResult = 0)]
        [TestCase("abc", ExpectedResult = 0)]
        [TestCase("12345", ExpectedResult = 0)]
        [TestCase("122345", ExpectedResult = 0)]
        [TestCase("abc abc abc 123 321", ExpectedResult = 0)]
        public int? CountMaxIdenticalLetters_IdenticalLettersDoNotExist_ReturnsZero(string line)
        {
            return CountMaxIdenticalLetters(line);
        }

        [TestCase("a", ExpectedResult = 1)]
        [TestCase(" aa bbb ", ExpectedResult = 3)]
        [TestCase(" aaa bbb ", ExpectedResult = 3)]
        [TestCase(" 111 bb ", ExpectedResult = 2)]
        [TestCase(" 111bbbbccaabbbbbb ", ExpectedResult = 6)]
        [TestCase("    cc ", ExpectedResult = 2)]
        public int? CountMaxIdenticalLetters_IdenticalLettersExist_ReturnsCorrectResults(string line)
        {
            return CountMaxIdenticalLetters(line);
        }

        [TestCase("", ExpectedResult = 0)]
        [TestCase("a", ExpectedResult = 0)]
        [TestCase("123", ExpectedResult = 0)]
        [TestCase("abcde", ExpectedResult = 0)]
        [TestCase("abbcccd", ExpectedResult = 0)]
        [TestCase("abc abc abc 123 321", ExpectedResult = 0)]
        public int? CountMaxIdenticalDigits_IdenticalDigitsDoNotExist_ReturnsZero(string line)
        {
            return CountMaxIdenticalDigits(line);
        }


        [TestCase("1", ExpectedResult = 1)]
        [TestCase(" 11 222 ", ExpectedResult = 3)]
        [TestCase(" 111 222 ", ExpectedResult = 3)]
        [TestCase(" aaa 22 ", ExpectedResult = 2)]
        [TestCase(" abccc221113333 ", ExpectedResult = 4)]
        [TestCase("    22 ", ExpectedResult = 2)]
        public int? CountMaxIdenticalDigits_IdenticalDigitsExist_ReturnsCorrectResults(string line)
        {
            return CountMaxIdenticalDigits(line);
        }
    }
}
